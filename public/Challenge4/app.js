const uRock = document.getElementById("userRock");
const uPaper = document.getElementById("userPaper");
const uScissor = document.getElementById("userScissor");
const cRock = document.getElementById("comRock");
const cPaper = document.getElementById("comPaper");
const cScissor = document.getElementById("comScissor");
const winBox = document.getElementById("box");
const inFo = document.getElementById("h1");
const refresh = document.getElementById("refresh");
const x = document.querySelector(".user-tool");
const addElement1 = [...document.getElementsByClassName("keys")];
const button = document.querySelector("button");

// Otak Komputer
function botThink() {
  var choices = ["Batu", "Gunting", "Kertas"];
  var randomChoices = Math.floor(Math.random() * 3);
  return choices[randomChoices];
}
function resultObject() {
  winBox.classList.add("winBox"), inFo.setAttribute("style", "font-size:36px; color:white;");
}
function resultDraw() {
  winBox.classList.add("drawBox");

  inFo.setAttribute("style", "font-size:36px; color:white;");
}

function win() {
  console.log("Player 1 Win");
  resultObject();
  inFo.innerText = "menang 😄";
}

function lose() {
  console.log("COM WIN");
  resultObject();

  inFo.innerText = "kalah😢";
}

function draw() {
  console.log("Draw");
  resultDraw();

  inFo.innerText = "Seri";
}

function gameCompare(pilihanUser) {
  const computerUser = botThink();
  console.log("Kamu memilih" + pilihanUser);
  console.log("sedangkan komputer memilih" + computerUser);

  switch (pilihanUser + computerUser) {
    case "BatuGunting":
    case "GuntingKertas":
    case "KertasBatu":
      win();

      break;
    case "GuntingBatu":
    case "BatuKertas":
    case "KertasGunting":
      lose();
      break;
    case "GuntingGunting":
    case "BatuBatu":
    case "KertasKertas":
      draw();
  }
  switch (computerUser) {
    case "Batu":
      cRock.classList.add("chosen");
      break;
    case "Gunting":
      cScissor.classList.add("chosen");
      break;
    case "Kertas":
      cPaper.classList.add("chosen");
  }
}
function play() {
  uRock.addEventListener("click", function () {
    this.classList.add("chosen");
    gameCompare("Batu");
    addElement1.forEach((addElement3) => {
      addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;");
    });
  });

  uPaper.addEventListener("click", function () {
    this.classList.add("chosen");
    gameCompare("Kertas");
    addElement1.forEach((addElement3) => {
      addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;");
    });
  });

  uScissor.addEventListener("click", function () {
    this.classList.add("chosen");
    gameCompare("Gunting");
    addElement1.forEach((addElement3) => {
      addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;");
    });
  });
}

play();

refresh.addEventListener("click", function () {
  addElement1.forEach((addElement2) => {
    addElement2.classList.remove("chosen");
  });
  addElement1.forEach((addElement3) => {
    addElement3.removeAttribute("style", "cursor: not-allowed;pointer-events: none;");
  });
  winBox.classList.remove("winBox");
  winBox.classList.remove("drawBox");
  inFo.removeAttribute("style", "color: ''; font-size:'' ");
  inFo.style.marginTop = null;
  inFo.style.fontSize = null;
  inFo.innerText = "VS";
  button.disabled = false;
});
