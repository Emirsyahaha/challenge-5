const express = require("express");
const path = require("path");
const forRouter = require("./forUsers/user.route");
const app = express();
const port = 3000;

app.use(express.json());
app.use("/", forRouter);
app.use(express.static("./public"));

app.get("/challenge3", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/Challenge3/index.html"));
});

app.get("/challenge4", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/Challenge4/suit.html"));
});

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
