'use strict';

const { DataTypes } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("userBios", "phoneNumber", {
      type: DataTypes.STRING,
    });
    await queryInterface.addColumn("userBios", "address", {
      type: DataTypes.STRING,
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("userBios", "phoneNumber");
    await queryInterface.removeColumn("userBios", "address");
  }
};
