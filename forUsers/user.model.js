const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

class UserModel {
  // untuk mendapatkan data semua user
  allUsers = async () => {
    return await db.user.findAll({ include: [db.userBio] });
  };

  newUserBio = async (userId) => {
    const bioExist = await db.userBio.findOne({
      where: {
        user_id: userId,
      },
    });
    return bioExist;
  };
  registerUserBio = async (userId, fullname, address, phoneNumber) => {
    return await db.userBio.create({
      fullname: fullname,
      address: address,
      phoneNumber: phoneNumber,
      user_id: userId,
    });
  };

  detailById = async (userId) => {
    return await db.user.findOne({ include: [db.userBio], where: { id: userId } });
  };

  updateUserBio = async (userId, fullname, address, phoneNumber) => {
    return await db.userBio.update({ fullname: fullname, address: address, phoneNumber: phoneNumber }, { where: { user_id: userId } });
  };

  // Untuk menambah data history game
  historyInput = (userGame) => {
    db.gameHistory.create({
      user_id: userGame.user_id,
      status: userGame.status,
    });
  };

  // untuk menampilkan history permainan saja
  gameHistories = async (userId) => {
    return await db.user.findOne({ include: [db.userBio, db.gameHistory], where: { id: userId } });
  };

  // untuk cek data sebelum didaftarkan
  forChecking = async (dataChecking) => {
    const dataExist = await db.user.findOne({
      where: { [Op.or]: [{ username: dataChecking.username }, { email: dataChecking.email }] },
    });
    return dataExist;
  };

  // untuk registrasi data baru
  newUser = (dataChecking) => {
    db.user.create({
      username: dataChecking.username,
      email: dataChecking.email,
      password: md5(dataChecking.password),
    });
  };

  // untuk login
  forLogin = async (username, email, password) => {
    const dataBody = await db.user.findOne({
      where: { username: username, email: email, password: md5(password) },
    });
    return dataBody;
  };
}

module.exports = new UserModel();
