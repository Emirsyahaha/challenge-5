const express = require("express");
const forRouter = express.Router();
const userControl = require("./user.control");

forRouter.get("/user", userControl.showAllUser);

forRouter.post("/registration", userControl.registerUser);

forRouter.post("/login", userControl.loginUser);

forRouter.put("/addUserBio/:userId", userControl.newUserBio);

forRouter.get("/userBio/:userId", userControl.detailById);

forRouter.put("/gameHistoryInput", userControl.historyInput);

forRouter.get("/gameHistory/:userId", userControl.gameHistories);

module.exports = forRouter;
