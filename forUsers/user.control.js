const userModel = require("./user.model");

class userControl {
  // Untuk menampilkan semua data user
  showAllUser = async (req, res) => {
    const allUsers = await userModel.allUsers();
    return res.json(allUsers);
  };

  newUserBio = async (req, res) => {
    const { userId } = req.params;
    const { fullname, address, phoneNumber } = req.body;
    const bioExist = await userModel.newUserBio(userId);

    try {
      if (bioExist) {
        userModel.updateUserBio(userId, fullname, address, phoneNumber);
        res.statusCode = 200;
        return res.json({ message: "update existing user bio success!!" });
      } else {
        userModel.registerUserBio(userId, fullname, address, phoneNumber);
        res.statusCode = 200;
        return res.json({ message: "add user bio success!!" });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "please register user first!!!" });
    }
  };

  // Untuk menampilkan data user spesifik
  detailById = async (req, res) => {
    const { userId } = req.params;
    try {
      const usersParams = await userModel.detailById(userId);

      if (usersParams) {
        res.statusCode = 200;
        return res.json(usersParams);
      } else {
        res.statusCode = 400;
        return res.json({ message: `User id tidak ditemukan : ${userId}` });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: `User id tidak ditemukan : ${userId}` });
    }
  };

  historyInput = async (req, res) => {
    const userGame = req.body;
    userModel.historyInput(userGame);

    return res.json({ message: "history updated!!" });
  };

  gameHistories = async (req, res) => {
    const { userId } = req.params;

    const gameHistories = await userModel.gameHistories(userId);
    return res.json(gameHistories);
  };

  // Untuk mendaftarkan user baru
  registerUser = async (req, res) => {
    const dataChecking = req.body;
    const dataRegistered = await userModel.forChecking(dataChecking);
    if (dataRegistered) {
      return res.json({ message: "Username / email already registered" });
    } else if (dataChecking.username === "" && dataChecking.email === "" && dataChecking.password === "") {
      res.json({ message: "please fill your personal data" });
    } else if (dataChecking.username === "" || dataChecking.username === undefined) {
      res.json({ message: "please fill your username" });
    } else if (dataChecking.email === "" || dataChecking.email === undefined) {
      res.json({ message: "please fill your email" });
    } else if (dataChecking.password === "" || dataChecking.password === undefined) {
      res.json({ message: "please fill your password" });
    } else userModel.newUser(dataChecking);
    return res.json({ message: "register complete" });
  };

  // Untuk mencocokkan data yang diinput user
  loginUser = async (req, res) => {
    const { username, email, password } = req.body;
    const chosenUsers = await userModel.forLogin(username, email, password);

    if (chosenUsers) {
      res.statusCode = 200;
      return res.json(chosenUsers);
    } else {
      res.statusCode = 400;
      return res.json({ message: "user is not exist!" });
    }
  };
}

module.exports = new userControl();
